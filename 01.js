const app = new Vue({
    el: '#App',
    data: {
        title: 'Hola mundo con Vue',
        fruits: [
            {name: 'Mango', quantity: 11},
            {name: 'Apple', quantity: 0},
            {name: 'Guayaba', quantity: 6}
        ],
        newFruit: '',
        total: 0
    },
    methods: {
        addFruit() {
            if(this.newFruit != '') {
                this.fruits.push({
                    name: this.newFruit, quantity: 0
                });
                this.newFruit = '';
            }
        }
    },
    computed: {
        sumFruits() {
            this.total = 0;
            for(let fruit of this.fruits) {
                this.total += fruit.quantity;
            }
            return this.total;
        }
    },
});